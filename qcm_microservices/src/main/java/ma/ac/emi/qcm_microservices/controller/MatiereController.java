package ma.ac.emi.qcm_microservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MatiereController {

    @GetMapping("/matiere")
    public String matieres(){
        return "matieres/matiere";
    }


    @GetMapping("/matiereUpdate")
    public String matiereUpdate(){
        return "matieres/matiereUpdate";
    }

}
