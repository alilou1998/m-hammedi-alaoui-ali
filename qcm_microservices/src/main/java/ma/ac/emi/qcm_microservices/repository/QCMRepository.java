package ma.ac.emi.qcm_microservices.repository;

import ma.ac.emi.qcm_microservices.entities.Classe;
import ma.ac.emi.qcm_microservices.entities.Formateur;
import ma.ac.emi.qcm_microservices.entities.Matiere;
import ma.ac.emi.qcm_microservices.entities.Qcm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface QCMRepository extends JpaRepository<Qcm,Long> {

    @Query("select qcm from Qcm qcm where qcm.matiere.id=:x")
    List<Qcm> findByMatiere(@Param("x")Long idMatiere);

    Qcm getQcmById(long id);

    @Query("select q from Qcm q where q.matiere=:x")
    List<Qcm> getQcmByMatiere(@Param("x") Matiere matiere);

    @Query("select q from Qcm q where q.classe=:x")
    List<Qcm> getQcmByClasse(@Param("x") Classe classe);

    @Query("select q from Qcm q where q.formateur=:x")
    List<Qcm> getQcmByFormateur(@Param("x") Formateur formateur);


}
