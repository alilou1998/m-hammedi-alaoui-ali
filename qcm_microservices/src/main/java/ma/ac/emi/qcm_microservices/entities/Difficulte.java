package ma.ac.emi.qcm_microservices.entities;

public enum Difficulte {
	Simple, Normale, Difficile
}
