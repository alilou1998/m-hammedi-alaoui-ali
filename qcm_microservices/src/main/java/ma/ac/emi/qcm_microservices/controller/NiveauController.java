package ma.ac.emi.qcm_microservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NiveauController {

    @GetMapping("/niveaux")
    public String niveaux(){
        return "niveaux/niveau";
    }

    @GetMapping("/niveauUpdate")
    public String niveauUpdate(){
        return "niveaux/niveauUpdate";
    }

}
