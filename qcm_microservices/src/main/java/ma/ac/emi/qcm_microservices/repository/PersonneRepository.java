package ma.ac.emi.qcm_microservices.repository;

import ma.ac.emi.qcm_microservices.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PersonneRepository extends JpaRepository<Personne, String> {

}
