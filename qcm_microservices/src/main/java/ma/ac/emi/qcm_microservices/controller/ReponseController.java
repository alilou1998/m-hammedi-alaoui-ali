package ma.ac.emi.qcm_microservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ReponseController {


    @GetMapping("/reponse")
    public String Answer(){
        return "reponses/repondre";
    }

}
