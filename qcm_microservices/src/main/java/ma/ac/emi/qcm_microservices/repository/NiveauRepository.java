package ma.ac.emi.qcm_microservices.repository;


import ma.ac.emi.qcm_microservices.entities.Niveau;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@RepositoryRestResource
public interface NiveauRepository extends JpaRepository<Niveau,Long> {
	@Query("select n from Niveau n where n.nom like:x")
    Page<Niveau> getPageNiveauByNom(@Param("x")String mc, Pageable pageable);

    @Query("select n from Niveau n JOIN n.formation as f where f.id=:x")
    List<Niveau> findByFormation(@Param("x") Long id);
}
