package ma.ac.emi.qcm_microservices.repository;

import ma.ac.emi.qcm_microservices.entities.Question;
import ma.ac.emi.qcm_microservices.entities.Theme;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface QuestionRepository  extends JpaRepository<Question,Long> {

    @Query("select q from Question q where q.id=:x")
    Question findByID(@Param("x")Long idQuestion);

    @Query(" select q from Question q where q.theme=:x")
    Page<Question> listQuestions(@Param("x") Theme theme, Pageable pageable);

}
