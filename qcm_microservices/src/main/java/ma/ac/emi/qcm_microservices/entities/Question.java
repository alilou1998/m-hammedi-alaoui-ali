package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String enonce;
	private Integer bareme;
	private Difficulte difficulte;
	private boolean test;
	private boolean partage;
	@ManyToOne
	@JoinColumn(name = "theme_id", nullable = false)
	private Theme theme;

	@ManyToOne
	@JoinColumn(name = "formateur_id", nullable = false)
	private Formateur formateur;
	@OneToMany(mappedBy = "question")
	private List<Reponse> reponses = new ArrayList<Reponse>();
	@ManyToMany(mappedBy = "questions")
	private List<Qcm> qcms = new ArrayList<>();

	public Question(String enonce,Integer bareme,Difficulte difficulte,boolean test,boolean partage,Theme theme,Formateur formateur) {
		this.enonce=enonce;
		this.bareme=bareme;
		this.difficulte=difficulte;
		this.test=test;
		this.partage=partage;
		this.theme=theme;
		this.formateur=formateur;
	}
}
