package ma.ac.emi.qcm_microservices.repository;


import ma.ac.emi.qcm_microservices.entities.Formateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface FormateurRepository extends JpaRepository<Formateur, String> {
    Formateur getFormateurByLogin(String login);
}
