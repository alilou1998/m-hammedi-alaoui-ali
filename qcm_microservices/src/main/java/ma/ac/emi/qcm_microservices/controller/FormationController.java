package ma.ac.emi.qcm_microservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FormationController {


    @GetMapping("/formation")
    public String formations(){
        return "formations/formation";
    }


    @GetMapping("/formationUpdate")
    public String formationUpdate(){
        return "formations/formationUpdate";
    }
}
