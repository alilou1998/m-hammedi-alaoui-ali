package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("ROLE_ELEVE")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Eleve extends Personne {

	private int matricule;

	@ManyToMany(mappedBy = "eleves")
	private List<Classe> classes = new ArrayList<Classe>();
	@OneToMany(mappedBy = "eleve")
	private List<Note> notes = new ArrayList<Note>();


	@Builder
	public Eleve(int matricule,String nom, String email, @Length(max = 90) String login, String password) {
		super(nom, email, login, password);
		this.matricule=matricule;
	}
}
