package ma.ac.emi.qcm_microservices;

import ma.ac.emi.qcm_microservices.entities.*;
import ma.ac.emi.qcm_microservices.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class QcmMicroservicesApplication implements CommandLineRunner {

    @Autowired
    EleveRepository eleveRepository;
    @Autowired
    FormateurRepository formateurRepository;
    @Autowired
    FormationRepository formationRepository;
    @Autowired
    NiveauRepository niveauRepository;
    @Autowired
    MatiereRepository matiereRepository;
    @Autowired
    ThemeRepository themeRepository;
    @Autowired
    ClasseRepository classeRepository;
    @Autowired
    QCMRepository qcmRepository;
    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    ReponseRepository reponseRepository;
    @Autowired
    RepositoryRestConfiguration repositoryRestConfiguration;

    public static void main(String[] args) {
        SpringApplication.run(QcmMicroservicesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        repositoryRestConfiguration.exposeIdsFor(Formation.class);
        repositoryRestConfiguration.exposeIdsFor(Classe.class);
        repositoryRestConfiguration.exposeIdsFor(Eleve.class);
        repositoryRestConfiguration.exposeIdsFor(Formateur.class);
        repositoryRestConfiguration.exposeIdsFor(Matiere.class);
        repositoryRestConfiguration.exposeIdsFor(Niveau.class);
        repositoryRestConfiguration.exposeIdsFor(Note.class);
        repositoryRestConfiguration.exposeIdsFor(Personne.class);
        repositoryRestConfiguration.exposeIdsFor(Qcm.class);
        repositoryRestConfiguration.exposeIdsFor(Question.class);
        repositoryRestConfiguration.exposeIdsFor(Reponse.class);
        repositoryRestConfiguration.exposeIdsFor(Theme.class);
        repositoryRestConfiguration.exposeIdsFor(Theme.class);

        Eleve eleve = new Eleve(1, "ali", "alimalaoui3@gmail.com", "ali", "1234");
        eleveRepository.save(eleve);
        Eleve eleve1 = new Eleve(2, "oussama", "oussama@gmail.com", "oussama", "1234");
        eleveRepository.save(eleve1);
        Eleve eleve2 = new Eleve(3, "bader", "bader@gmail.com", "bader", "1234");
        eleveRepository.save(eleve2);


        Formateur formateur = new Formateur("kabbaj", "kabbaj@gmail.com", "kabbaj", "1234");
        formateurRepository.save(formateur);
        Formateur formateur1 = new Formateur("anwar", "anwar@gmail.com", "anwar", "1234");
        formateurRepository.save(formateur1);


        Formation formation = new Formation("Ingenieurs");
        formationRepository.save(formation);
        Formation formation1 = new Formation("Doctorale");
        formationRepository.save(formation1);


        Niveau niveau1 = new Niveau("1ere annee", formation);
        niveauRepository.save(niveau1);
        Niveau niveau2 = new Niveau("2eme annee", formation);
        niveauRepository.save(niveau2);
        Niveau niveau3 = new Niveau("3eme annee", formation);
        niveauRepository.save(niveau3);

        Matiere matiere = new Matiere("JEE", niveau2);
        matiereRepository.save(matiere);
        Matiere matiere2 = new Matiere("Design Pattern", niveau2);
        matiereRepository.save(matiere2);

        Theme theme = new Theme(null, "Cours", matiere, null);
        themeRepository.save(theme);
        Theme theme1 = new Theme(null, "Culture", matiere2, null);
        themeRepository.save(theme1);

        Classe classe = new Classe("G INF 2021", niveau2);
        classe.getFormateurs().add(formateur);
        classe.getFormateurs().add(formateur1);
        classeRepository.save(classe);

        formateur.getClasses().add(classe);
        formateur1.getClasses().add(classe);
        formateurRepository.save(formateur);
        formateurRepository.save(formateur1);

        List<String> consignes = Arrays.asList("Ne pas tricher", "Documents autorisés");

        Qcm qcm1 = new Qcm("QCM1", consignes, Type.Normale, Mode.VraixFaux, false, false, classe, formateur, matiere);
        Qcm qcm2 = new Qcm("QCM2", consignes, Type.Penalisant, Mode.VraixFaux, false, false, classe, formateur, matiere);
        Qcm qcm3 = new Qcm("QCM3", consignes, Type.Normale, Mode.MultiChoix, false, false, classe, formateur1, matiere2);
        Qcm qcm4 = new Qcm("QCM4", consignes, Type.Normale, Mode.DragAndDrop, false, false, classe, formateur1, matiere2);

        qcm1.setMatiere(matiere);
        qcm2.setMatiere(matiere);
        qcm3.setMatiere(matiere2);
        qcm4.setMatiere(matiere2);

        qcmRepository.save(qcm1);
        qcmRepository.save(qcm2);
        qcmRepository.save(qcm3);
        qcmRepository.save(qcm4);

        Question question1 = new Question("Definition de ORM", 3, Difficulte.Normale, false, false, theme,
                formateur);
        Question question2 = new Question("Definition d'un EntityManager", 3, Difficulte.Difficile, false, false,
                theme, formateur);
        Question question3 = new Question("Definir Composite", 3, Difficulte.Normale, false, false, theme1,
                formateur1);

        question1.getQcms().add(qcm1);
        question2.getQcms().add(qcm1);
        question3.getQcms().add(qcm4);

        questionRepository.save(question1);
        questionRepository.save(question2);
        questionRepository.save(question3);


        qcm1.getQuestions().add(question1);
        qcm1.getQuestions().add(question2);
        qcm4.getQuestions().add(question3);


        qcmRepository.save(qcm1);
        qcmRepository.save(qcm2);
        qcmRepository.save(qcm4);

        Reponse reponse1 = new Reponse("The first question", true, question1);
        Reponse reponse2 = new Reponse("The second question", false, question2);
        Reponse reponse3 = new Reponse("En génie logiciel, un patron composite est un patron de conception structurel. Ce patron permet de concevoir une structure arborescente", true, question3);

        reponse3.setPropositions(Arrays.asList("structurel","creationnel","comportemental"));

        reponseRepository.save(reponse1);
        reponseRepository.save(reponse2);
        reponseRepository.save(reponse3);


    }
}
