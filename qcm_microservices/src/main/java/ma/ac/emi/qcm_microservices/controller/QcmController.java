package ma.ac.emi.qcm_microservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class QcmController {
    @GetMapping("/qcm")
    public String qcm(){
        return "qcms/listeQcm";
    }
    @GetMapping("/qcmInfos")
    public String qcmInfos(){
        return "qcms/infosQcm";
    }
}
