package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Niveau {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	@OneToMany(mappedBy = "niveau")
	private List<Classe> classes = new ArrayList<Classe>();
	@OneToMany(mappedBy = "niveau")
	private List<Matiere> matieres = new ArrayList<Matiere>();
	@ManyToOne
	@JoinColumn(name = "formation_id")
	private Formation formation;

	public Niveau(String nom,Formation formation) {
		this.nom=nom;
		this.formation=formation;
	}
}
