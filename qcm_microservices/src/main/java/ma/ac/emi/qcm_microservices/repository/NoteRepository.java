package ma.ac.emi.qcm_microservices.repository;

import ma.ac.emi.qcm_microservices.entities.Eleve;
import ma.ac.emi.qcm_microservices.entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface NoteRepository extends JpaRepository<Note, Long> {

	List<Note> findByEleve(Eleve eleve);
	Optional<Note> findById(Long id);
	

}
