package ma.ac.emi.qcm_microservices.repository;

import ma.ac.emi.qcm_microservices.entities.Question;
import ma.ac.emi.qcm_microservices.entities.Reponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ReponseRepository extends JpaRepository<Reponse,Long> {

    @Query(" select r from Reponse r where r.question=:x")
    Page<Reponse> listReponses(@Param("x") Question question, Pageable pageable);
}
