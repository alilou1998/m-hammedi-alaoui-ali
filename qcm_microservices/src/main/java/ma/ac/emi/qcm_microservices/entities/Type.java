package ma.ac.emi.qcm_microservices.entities;

public enum Type {
	Normale, Symetrique, Double, Penalisant, OmissionValorisee
}
