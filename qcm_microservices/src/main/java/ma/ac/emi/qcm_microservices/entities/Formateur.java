package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("ROLE_Formateur")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Formateur extends Personne {

    @ManyToMany(mappedBy = "formateurs")
    private List<Classe> classes = new ArrayList<Classe>();

    @OneToMany(mappedBy = "formateur")
    private List<Qcm> qcms = new ArrayList<Qcm>();

    @ManyToMany
    @JoinTable(name = "formateurs_matieres", joinColumns = {
            @JoinColumn(name = "formateur_id", referencedColumnName = "login", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "matiere_id", referencedColumnName = "id", nullable = false, updatable = false)})
    private List<Matiere> matieres = new ArrayList<Matiere>();
    @OneToMany(mappedBy = "formateur")
    private List<Question> questions;

    @Builder
    public Formateur(String nom, String email, @Length(max = 90) String login, String password) {
        super(nom, email, login, password);
    }

}
