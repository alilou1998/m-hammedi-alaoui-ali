package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Qcm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;
    @ElementCollection
    private List<String> consignes;
    private Type type;
    private Mode mode;
    private boolean test;
    private boolean partage;
    @ManyToOne
    @JoinColumn(name = "classe_id", nullable = false)
    private Classe classe;
    @ManyToOne
    @JoinColumn(name = "formateur_id", nullable = false)
    private Formateur formateur;

    @ManyToMany
    @JoinTable(name = "qcms_questions", joinColumns = {
            @JoinColumn(name = "qcm_id", referencedColumnName = "id", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "question_id", referencedColumnName = "id", nullable = false, updatable = false)})
    private List<Question> questions = new ArrayList<Question>();
    @ManyToOne
    @JoinColumn(name = "matiere_id")
    private Matiere matiere;

	public Qcm(String nom, List<String> consignes, Type type, Mode mode, boolean test, boolean partage, Classe classe, Formateur formateur, Matiere matiere) {
		this.nom=nom;
		this.consignes=consignes;
		this.type=type;
		this.test=test;
		this.mode=mode;
		this.partage=partage;
		this.classe=classe;
		this.formateur=formateur;
		this.matiere=matiere;
	}
}
