package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reponse {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String reponse;
	private boolean correcte;

	@ManyToOne
	@JoinColumn(name = "question_id", nullable = false)
	private Question question;
	@ElementCollection
	private List<String> propositions = new ArrayList<>();

	public Reponse(String reponse,boolean correcte,Question question) {
		this.correcte=correcte;
		this.reponse=reponse;
		this.question=question;
	}
}
