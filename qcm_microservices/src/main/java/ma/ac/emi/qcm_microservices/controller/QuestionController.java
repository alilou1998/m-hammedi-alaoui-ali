package ma.ac.emi.qcm_microservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class QuestionController {

    @GetMapping("/question")
    public String getQuestions(){
        return "questions/qsts";
    }

    @GetMapping("/questionsUpdate")
    public String questionUpdate(){
        return "questions/EditQuestion";
    }

}
