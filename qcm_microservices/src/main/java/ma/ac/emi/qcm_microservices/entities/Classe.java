package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    @ManyToMany
    @JoinTable(name = "classes_eleves", joinColumns = {
            @JoinColumn(name = "classe_id", referencedColumnName = "id", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "eleve_id", referencedColumnName = "login", nullable = false, updatable = false)})
    private List<Eleve> eleves = new ArrayList<Eleve>();
    @ManyToMany
    @JoinTable(name = "classes_formateurs", joinColumns = {
            @JoinColumn(name = "classe_id", referencedColumnName = "id", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "formateur_id", referencedColumnName = "login", nullable = false, updatable = false)})
    private List<Formateur> formateurs = new ArrayList<Formateur>();
    @ManyToOne
    private Niveau niveau;
    @OneToMany(mappedBy = "classe")
    private List<Qcm> qcms = new ArrayList<Qcm>();

    public Classe(String nom,Niveau niveau) {
        this.nom=nom;
        this.niveau=niveau;
    }
}
