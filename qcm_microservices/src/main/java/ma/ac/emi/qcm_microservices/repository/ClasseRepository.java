package ma.ac.emi.qcm_microservices.repository;


import ma.ac.emi.qcm_microservices.entities.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ClasseRepository extends JpaRepository<Classe,Long> {
    Classe getClasseById(Long id);
}
