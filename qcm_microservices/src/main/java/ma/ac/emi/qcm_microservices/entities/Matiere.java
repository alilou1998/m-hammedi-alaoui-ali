package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Matiere {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	@ManyToMany(mappedBy = "matieres")
	private List<Formateur> formateurs;
	@ManyToOne
	@JoinColumn(name = "niveau_id")
	private Niveau niveau;
	@OneToMany(mappedBy = "matiere")
	private List<Theme> themes = new ArrayList<Theme>();
	@OneToMany(mappedBy = "matiere")
	private List<Qcm> examens = new ArrayList<Qcm>();


	public Matiere(String nom,Niveau niveau) {
		this.nom=nom;
		this.niveau=niveau;
	}
}
