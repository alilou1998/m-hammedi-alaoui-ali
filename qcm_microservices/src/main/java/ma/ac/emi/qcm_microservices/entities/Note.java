package ma.ac.emi.qcm_microservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Note {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer note;
	@ManyToOne
	@JoinColumn(name = "Eleve_id", nullable = false)
	private Eleve eleve;
	@OneToMany
	@JoinTable(name = "note_question")
	private List<Question> questions = new ArrayList<Question>();
	@OneToOne
	private Qcm qcm;


}
